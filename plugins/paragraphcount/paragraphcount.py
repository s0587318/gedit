import re
from gi.repository import GObject, Gtk, Gedit # pylint: disable=E0611

# Regular expression to match paragraphs. A paragraph is any non-empty text block
# separated by one or more newline characters.
PARAGRAPH_RE = re.compile(r'(?:\S.*?(?:\n|\Z))+', re.MULTILINE)

def get_text(doc):
    """
    Return the full text of the document.
    
    Args:
        doc: The document from which to retrieve text.

    Returns:
        The full text of the document as a string.
    """
    start, end = doc.get_bounds()
    return doc.get_text(start, end, False)

class ParagraphcountPlugin(GObject.Object, Gedit.WindowActivatable):
    """
    Adds a Label to the status bar with the active document's paragraph count,
    where a paragraph is defined as any text separated by one or more newline characters.
    """
    __gtype_name__ = "paragraphcount"
    window = GObject.property(type=Gedit.Window)
    
    def __init__(self):
        """Initialize the plugin."""
        GObject.Object.__init__(self)
        self._doc_changed_id = None
        self._label = Gtk.Label()
    
    def do_activate(self):
        """Called when the plugin is activated."""
        # Add the label to the status bar
        self.window.get_statusbar().pack_end(self._label, False, False, 5)
        self._label.show()
    
    def do_deactivate(self):
        """Called when the plugin is deactivated, performs cleanup."""
        # Remove the label from the status bar
        Gtk.Container.remove(self.window.get_statusbar(), self._label)
        # Disconnect the document change signal if connected
        if self._doc_changed_id:
            self._doc_changed_id[0].disconnect(self._doc_changed_id[1])
        del self._label
    
    def do_update_state(self):
        """Called when the plugin state requires an update."""
        # Disconnect the previous document change signal if connected
        if self._doc_changed_id:
            self._doc_changed_id[0].disconnect(self._doc_changed_id[1])
        # Get the currently active document
        doc = self.window.get_active_document()
        if doc:
            # Connect to the document's "changed" signal to update the paragraph count
            self._doc_changed_id = (doc, doc.connect("changed", self.on_document_changed))
            # Update the label with the current paragraph count
            self.update_label(doc)
        else: # If no document is open (user closed all tabs), clear the label
            self._label.set_text('')
    
    def on_document_changed(self, doc):
        """Called when the active document's content has changed."""
        self.update_label(doc)
        
    def update_label(self, doc):
        """
        Update the plugin's status bar label with the current paragraph count.
        
        Args:
            doc: The document for which to update the paragraph count.
        """
        # Get the text of the document
        txt = get_text(doc)
        # Count the paragraphs using the regular expression
        paragraph_count = len(PARAGRAPH_RE.findall(txt))
        # Update the label with the paragraph count
        msg = 'paragraphs: {0}'.format(paragraph_count)
        self._label.set_text(msg)

