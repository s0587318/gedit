"""
Change the opacity of the window
"""

from gi.repository import GObject, Gedit, Gtk, Gio  # pylint: disable=E0611
from .methods import Methods
from .tests import TestMethods


class WindowtransparencyPlugin(GObject.Object, Gedit.WindowActivatable):
    """
    Adds a Label to the status bar with the result
    """
    __gtype_name__ = "windowtransparency"
    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        """called when plugin is activated"""

        # Run Tests
        self.run_tests()

        Methods.set_opacity_m(self, self.window, 0.7)

    def do_deactivate(self):
        """called when plugin is deactivated, reset"""
        Methods.set_opacity_m(self, self.window, 1.0)

    def run_tests(self):
        """Unittests"""
        TestMethods.test_set_opacity_low(self)
        TestMethods.test_set_opacity_high(self)
