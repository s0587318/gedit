from gi.repository import GObject, Gedit, Gtk, Gio  # pylint: disable=E0611


class Methods():

    def set_opacity_m(self, window, opacity):
        """Set the opacity of the window"""
        window.set_opacity(opacity)
