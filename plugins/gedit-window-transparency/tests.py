from unittest import TestCase
from .methods import Methods


class TestMethods(Methods):

    def __init__(self):
        GObject.Object.__init__(self)
        self.window = None

    def test_set_opacity_low(self):
        Methods.set_opacity_m(self, self.window, 0.7)
        opacity = self.window.get_opacity()
        TestCase().assertEqual(opacity, 0.7019607843137254, "Test Failed!")

    def test_set_opacity_high(self):
        Methods.set_opacity_m(self, self.window, 1.0)
        opacity = self.window.get_opacity()
        TestCase().assertEqual(opacity, 1.0, "Test Failed!")
