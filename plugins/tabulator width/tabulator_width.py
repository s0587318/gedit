from gi.repository import GObject, Gedit, Gio

class TabulatorPlugin(GObject.Object, Gedit.WindowActivatable):
    __gtype_name__ = "TabulatorPlugin"
    window = GObject.Property(type=Gedit.Window)

    def __init__(self):
        # Initialize the base class
        super().__init__()

    def do_activate(self):
        # Connect to the documents when the plugin is activated
        self._connect_to_documents()

    def do_deactivate(self):
        # Set tab width to 8 when the plugin is deactivated
        self._change_tab_width(8)

    def do_update_state(self):
        # No state update functionality is required
        pass

    def _connect_to_documents(self):
        # Connect the 'changed' signal of each document to the on_document_changed handler
        for doc in self.window.get_documents():
            doc.connect('changed', self.on_document_changed)

    def on_document_changed(self, doc):
        # Get the entire text of the document
        text = doc.get_text(doc.get_start_iter(), doc.get_end_iter(), True)
        # Extract the tab length from the text
        tab_length = self._extract_tab_length(text)
        # Change the tab width based on the extracted tab length
        self._change_tab_width(tab_length)

    def _extract_tab_length(self, text):
        # Search for 'tab = ' in the text and extract the integer value following it
        try:
            start_index = text.index('tab = ') + len('tab = ')
            end_index = start_index
            # Increment end_index to include all consecutive digits
            while end_index < len(text) and text[end_index].isdigit():
                end_index += 1
            return int(text[start_index:end_index])
        except ValueError:
            # Return default tab width of 8 if the conversion to int fails
            return 8
        except IndexError:
            # Return default tab width of 8 if 'tab = ' is not found in the text
            return 8

    def _change_tab_width(self, length):
        # Set the tab width for all views in the window
        for view in self.window.get_views():
            view.set_tab_width(length)
        # Update the GSettings with the new tab width
        self._update_gsettings(length)

    def _update_gsettings(self, length):
        # Save the tab width setting using GSettings
        schema = 'org.gnome.gedit.preferences.editor'
        key = 'tabs-size'
        gsettings = Gio.Settings(schema)
        gsettings.set_uint(key, length)

